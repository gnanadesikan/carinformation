package com.example.viewactivity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.model.CarData;
import java.util.List;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.MyViewHolder> {
    Context context;
    List<CarData> carDataList;
    private View view;

    public CarAdapter(Context context, List<CarData> carDataList) {
        this.context = context;
        this.carDataList = carDataList;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        view = layoutInflater.inflate(R.layout.recyclerview_caritem, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final CarData carShowObject = carDataList.get(i);
        if (carShowObject != null){
            myViewHolder.carMake.setText(carShowObject.getMake());
        myViewHolder.carModel.setText(carShowObject.getModel());
        myViewHolder.carShowName.setText(carShowObject.getShowName());
        }
        if (i%2==1){
            myViewHolder.mainRL.setBackgroundColor(context.getResources().getColor(R.color.gray));
        }
        else {
            myViewHolder.mainRL.setBackgroundColor(context.getResources().getColor(R.color.gray_lesser_dark));
        }
    }

    @Override
    public int getItemCount() {
        return carDataList.size() ;
   }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //Mapping the car list item views
        TextView carMake;
        TextView carModel;
        TextView carShowName;
        LinearLayout mainRL;

        public MyViewHolder(View itemView) {
            super(itemView);
            carMake = itemView.findViewById(R.id.car_make);
            carModel = itemView.findViewById(R.id.car_model);
            carShowName = itemView.findViewById(R.id.car_show_name);
            mainRL=itemView.findViewById(R.id.mainRL);
        }
    }
}
