package com.example.viewactivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.model.CarData;
import com.example.model.CarMain;
import com.example.presenter.CarDetailsFetchPresenter;
import com.example.utilities.CarScreenView;
import com.example.utilities.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CarMainScreenActivity extends AppCompatActivity implements CarScreenView {


    CarDetailsFetchPresenter carDetailsFetchPresenter;
    CarAdapter carAdapter;

    @BindView(R.id.getcarinfo_button)
    Button getCarDetailsButton;

    @BindView(R.id.carRecyclerView)
    RecyclerView carDetailsRecyclerView;

    @BindView(R.id.headerLL)
    LinearLayout headerLL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carmain);
        ButterKnife.bind(this);
        Constants.resetNetworkInstance();
        carDetailsFetchPresenter = new CarDetailsFetchPresenter(this, this);

    }

    @OnClick(R.id.getcarinfo_button)
    void callCarFetchWebService(){
        carDetailsFetchPresenter.callCarDetailsFetchServiceCall();
    }

    @Override
    public void onSuccessResponse(List<CarData> carDataList) {
        if(carDataList!=null) {
            getCarDetailsButton.setVisibility(View.GONE); // To hide the button onsuccessfull response
            headerLL.setVisibility(View.VISIBLE); //SETTING THE TITLE
            carDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(this)); //set managaer instace on recycler view
            carDetailsRecyclerView.setHasFixedSize(true);
            carAdapter = new CarAdapter(this, carDataList);
            carDetailsRecyclerView.setAdapter(carAdapter);
        }
        else{
            getCarDetailsButton.setVisibility(View.VISIBLE); // To hide the button onsuccessfull response
        }
    }

    @Override
    public void onFailureResponse(String message) {
        //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Light_Dialog);
        builder.setCancelable(false).setMessage(message)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

}
