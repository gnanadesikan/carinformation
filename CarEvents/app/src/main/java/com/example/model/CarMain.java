package com.example.model;




import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
//Created List for carshow Since JSON is not coming in expected format
public class CarMain  {
    @SerializedName("carslist")
    @Expose
    private List<CarShow> carslist = null;

    public List<CarShow> getCarslist() {
        return carslist;
    }

    public void setCarslist(List<CarShow> carslist) {
        this.carslist = carslist;
    }

}
