package com.example.utilities.networkutilities;

import android.content.Context;

import com.example.viewactivity.R;
import com.example.utilities.Constants;
import com.example.utilities.GenericResponseListener;


import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.utilities.Constants.SUCCESS_RESP_CODE;

public class RequestManager {

    private Context context;

    public RequestManager(Context context) {
        this.context = context;
    }

    public <T> void callRetrofit(final int requestCode, Call<T> call, final GenericResponseListener responseListener) {
        if (Constants.isNetworkAvailable(context)) {

            try {
                call.enqueue(new Callback<T>() {
                    @Override
                    public void onResponse(Call<T> call, Response<T> response) {

                       if(response.code()==200){
                           Object obj = readJson();   // Temporarily taking the value from offline JSOn file as the API JSON is having InvalidFormat JSON to fetch the data
                           responseListener.onSuccess(requestCode, response.code(), obj);
                       }else{
                           responseListener.onSuccess(requestCode, response.code(), response.body());
                       }
                    }

                    @Override
                    public void onFailure(Call<T> call, Throwable t) {

                        Object obj = readJson();   // Temporarily taking the value from offline JSOn file as the API JSON is having InvalidFormat JSON to fetch the data
                        responseListener.onSuccess(requestCode, SUCCESS_RESP_CODE, obj);

                        //Uncomment When API is returning valid response
                       //responseListener.onError(context.getString(R.string.generic_error));
                    }
                });
            } catch (Exception e) {
                responseListener.onError(context.getString(R.string.generic_error));
            }
        } else {
            responseListener.onError(context.getString(R.string.no_internet_connection));
        }
    }


    public <T> Object readJson() {

        try {
            JSONObject obj = new JSONObject(readJSONFromAsset());
            return obj;
        }
        catch (Exception e){
            return null;
        }

    }
    public String readJSONFromAsset(){ //Reading the JSON data from the asset folder for local data display
        String json = null;
        try {
            InputStream is = context.getAssets().open("sampleresp.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
