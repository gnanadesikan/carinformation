package com.example.utilities;

import com.example.model.CarData;
import com.example.model.CarMain;

import java.util.List;


public interface CarScreenView {
    void onSuccessResponse(List<CarData> carDataList);

    void onFailureResponse(String message);
}
