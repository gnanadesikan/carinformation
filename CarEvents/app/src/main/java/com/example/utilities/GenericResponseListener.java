package com.example.utilities;

public interface GenericResponseListener {

    void onSuccess(int requestCode, int responseCode, Object responseObject);

    void onError(String errorData);
}
