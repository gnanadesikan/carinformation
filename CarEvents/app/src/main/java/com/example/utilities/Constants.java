package com.example.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.viewactivity.BuildConfig;
import com.example.utilities.networkutilities.NetworkInitiator;

public class Constants {

    //CONNECTION TIMEOUTS
    public static final int API_CONNECT_TIMEOUT = 180; // in secs
    public static final int API_READ_TIMEOUT = 180; // in secs
    public static final String STAGE_BASE_URL ="http://eacodingtest.digital.energyaustralia.com.au";
    public static final String PROD_BASE_URL = "http://eacodingtest.digital.energyaustralia.com.au";
    public static int CAR_DETAIL_FETCH_REQUEST_CODE = 10;

    //RESPONSE CODE
    public static int SUCCESS_RESP_CODE=200;

    //Car API Service Method Utility
    public static CarApiService getCarApiService(Context context) {
        if (BuildConfig.DEBUG) {
            NetworkInitiator.setBaseURL(STAGE_BASE_URL);
        } else {
            NetworkInitiator.setBaseURL(PROD_BASE_URL);
        }
        NetworkInitiator initiator = NetworkInitiator.getInstance(context, CarApiService.class);
        return (CarApiService) initiator.initiateNetwork();
    }
    public static void resetNetworkInstance() {
        NetworkInitiator.resetInstance();
    }

    //NetWork Availability Check
    public static boolean isNetworkAvailable(Context context) {
        if (null != context) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnected();
        }
        return false;
    }


}
