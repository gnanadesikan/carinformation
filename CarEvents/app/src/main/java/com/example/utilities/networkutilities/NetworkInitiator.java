package com.example.utilities.networkutilities;

import android.content.Context;


import com.example.viewactivity.BuildConfig;
import com.example.utilities.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkInitiator {

    private static NetworkInitiator INSTANCE;
    private static String BASE_URL;
    private final Class<?> apiService;
    private Context context;
    private static Object retrofit = null;

    private NetworkInitiator(Context context, String baseURL, Class<?> apiService) {
        BASE_URL = baseURL;
        this.apiService = apiService;
        this.context = context;
    }

    public static NetworkInitiator getInstance(Context context, Class<?> apiService) {
        if (null == INSTANCE) {
            INSTANCE = new NetworkInitiator(context, BASE_URL, apiService);
        }
        return INSTANCE;
    }

    public static void resetInstance() {
        INSTANCE = null;
    }

    public static void setBaseURL(String baseURL) {
        BASE_URL = baseURL;
    }

    public Object initiateNetwork() {

        int cacheSize = 5 * 1024 * 1024; // 5 MB
        Cache cache = new Cache(context.getCacheDir(), cacheSize);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(Constants.API_READ_TIMEOUT, TimeUnit.SECONDS);
        httpClient.connectTimeout(Constants.API_CONNECT_TIMEOUT, TimeUnit.SECONDS);

        // add cache
        httpClient.cache(cache);

        if(BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            //log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build().create(apiService);

    }

}
