package com.example.utilities;

import com.example.model.CarMain;
import retrofit2.Call;
import retrofit2.http.GET;


public interface CarApiService {

    //@GET("/bins/j8x16")
    @GET("/api/v1/cars")
    Call<CarMain> getCarEventDetails();
}
