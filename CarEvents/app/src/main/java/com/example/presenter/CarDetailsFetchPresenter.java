package com.example.presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.model.CarData;
import com.example.viewactivity.R;
import com.example.model.CarMain;
import com.example.utilities.CarApiService;
import com.example.utilities.CarScreenView;
import com.example.utilities.Constants;
import com.example.utilities.GenericResponseListener;
import com.example.utilities.networkutilities.NetworkInitiator;
import com.example.utilities.networkutilities.RequestManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;

import static com.example.utilities.Constants.SUCCESS_RESP_CODE;

public class CarDetailsFetchPresenter implements GenericResponseListener {
    private final Context context;
    private ProgressDialog mDialog;
    private RequestManager requestManager;
    private CarApiService carApiService;
    CarScreenView carScreenView;

    public CarDetailsFetchPresenter(Context context, CarScreenView carScreenView) {
        this.context = context;
        this.carScreenView = carScreenView;
        requestManager = new RequestManager(context);
        NetworkInitiator.resetInstance();
        carApiService = Constants.getCarApiService(context);
    }

    public void callCarDetailsFetchServiceCall() {
        showProgress(context);

        Call<CarMain> getCarDetail = carApiService.getCarEventDetails();
        requestManager.callRetrofit(Constants.CAR_DETAIL_FETCH_REQUEST_CODE, getCarDetail, this);
    }


    @Override
    public void onSuccess(int requestCode, int responseCode, Object responseObject) {
        mDialog.dismiss();

        if (Constants.CAR_DETAIL_FETCH_REQUEST_CODE == requestCode) {
            if (responseCode == SUCCESS_RESP_CODE) {
                Type listType =new TypeToken<CarMain>(){}.getType(); //Remove this part of code when API is returning valid data
                CarMain carMainObject = new Gson().fromJson(responseObject.toString(),listType);
                mDialog.dismiss();
                carScreenView.onSuccessResponse(formatAndSortList(carMainObject));


            } else {
                mDialog.dismiss();
                carScreenView.onFailureResponse(context.getString(R.string.generic_error));
                //Toast.makeText(context, "Bad Request", Toast.LENGTH_LONG).show();
            }
        }
        else{
            mDialog.dismiss();
            carScreenView.onFailureResponse(context.getString(R.string.generic_error));
        }
    }

    @Override
    public void onError(String errorData) {
        mDialog.dismiss();
        carScreenView.onFailureResponse(errorData);
        // Toast.makeText(context, errorData, Toast.LENGTH_SHORT).show();
    }


    //RESPONSE DATA FORMAT AND SORTING
    List<CarData> formatAndSortList(CarMain carMainObject){
        List<CarData> carDataList = new ArrayList<>();
        for (int i = 0; i < carMainObject.getCarslist().size(); i++) { //COMBING ALL THE CAR DATA INTO A SINGLE LIST WITH ALL MAKES FOR POPULATING IN RECYCLERVIEW
            for (int j = 0; j < carMainObject.getCarslist().get(i).getCars().size(); j++) {
                CarData carData = new CarData();
                carData.setMake(carMainObject.getCarslist().get(i).getCars().get(j).getMake());
                carData.setModel(carMainObject.getCarslist().get(i).getCars().get(j).getModel());
                carData.setShowName(carMainObject.getCarslist().get(i).getName());
                carDataList.add(carData);
            }
        }
        //SORTING ALPHABETICALLY
        Collections.sort(carDataList, new Comparator<CarData>() {
            public int compare(CarData val1, CarData val2) {
                String data1 = new String(val1.getMake());
                String data2 = new String(val2.getMake());
                return data1.compareTo(data2);
            }
        });

        return carDataList;
    }

    private void showProgress(Context context) {
        mDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        mDialog.setMessage("Please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.show();
    }
}
