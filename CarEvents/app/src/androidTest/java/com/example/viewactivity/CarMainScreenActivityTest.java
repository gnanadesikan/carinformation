package com.example.viewactivity;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import com.example.utilities.Constants;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;


public class CarMainScreenActivityTest {
    public Context appTestContext;

    @Rule
    public ActivityTestRule<CarMainScreenActivity> activityTestRule= new ActivityTestRule<>(CarMainScreenActivity.class);

    @Before
    public void initSetUp(){
        appTestContext = InstrumentationRegistry.getContext();
    }

    @Test
    public void testActivity(){
        if(!Constants.isNetworkAvailable(appTestContext)){
            onView(withText(R.string.no_internet_connection))
                    .inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView()))))
                    .check(matches(isDisplayed()));
        }
    }


    @Test
    public void testButton(){
        onView(withId(R.id.getcarinfo_button)).check(matches(withText("GET CAR INFORMATION")));
    }

    @Test
    public void testButtonClick(){
        onView(withId(R.id.getcarinfo_button)).check(matches(isClickable())).perform(ViewActions.click());
    }

    @Test
    public void testCarList(){
        onView(withId(R.id.carRecyclerView)).check(matches(isDisplayed()));
    }

    @Test
    public void testCarListRows(){
        RecyclerView recyclerView=activityTestRule.getActivity().findViewById(R.id.carRecyclerView);
        int rowCounts=recyclerView.getAdapter().getItemCount();
        assertTrue(rowCounts>0);
    }

}
